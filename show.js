(function() {
  var storage = new CustomStorage();
  var pullrequestId;

  $(() => {
    match = window.location.pathname.match(/^\/?(?<account>[^\/]+)\/(?<repo>[^\/]+)\/pull-requests\/(?<pullrequestId>\d+)/);
    if (match == null) {
      return;
    }
    pullrequestId = match.groups.pullrequestId;

    var timer = setInterval(function() {
      if ($('#compare').length == 0) return;

      clearInterval(timer);
      storage.sync(() => {
        var url = `https://bitbucket.org/!api/2.0/repositories/${match.groups.account}/${match.groups.repo}/pullrequests/${pullrequestId}/comments?pagelen=100`
        getComments(url);
      });
    }, 500);

    subscribeToStatusLinks();
    subscribetoNavigationLinks();
    subscribeToDomChanges();
  });

  function getComments(url) {
    $.ajax({
      method: "GET",
      url: url,
      success: (data) => {
        if (data.next) {
          getComments(data.next);
        }
        var statuses = storage.getComments(pullrequestId);
        if (statuses == null) {
          statuses = {};
        }
        initCommentStatuses(data.values, statuses);
        renderLinks(data.values, statuses);
        storage.setComments(pullrequestId, statuses).flush();
        resolveOutdatedComments(data.values, statuses);
      }
    });
  }

  function initCommentStatuses(comments, statuses) {
    comments.forEach((comment) => {
      if (statuses[comment.id] == null) statuses[comment.id] = 'new';
      if (comment.deleted) delete statuses[comment.id];
    });
  }

  function renderLinks(comments, statuses) {
    comments.forEach((comment) => Renderer.renderCommentActions(pullrequestId, comment.id, statuses[comment.id]));
  }

  function resolveOutdatedComments(comments, statuses) {
    var unresolvedComments = comments.filter((comment) => statuses[comment.id] != 'resolved');
    var deferreds = unresolvedComments.map((comment) => comment.deferred = $.Deferred())
    Promise.all(deferreds).then(() => storage.flush());
    unresolvedComments.map((comment) => resolveCommentIfOutdated(comment));
  }

  function resolveCommentIfOutdated(comment) {
    return $.ajax({
      method: "GET",
      url: comment.links.self.href,
      success: (data) => {
        if (data.inline && data.inline.outdated) {
          var comments = storage.getComments(comment.pullrequest.id)
          comments[comment.id] = 'resolved';
          storage.setComments(comment.pullrequest.id, comments);
        }
        if (comment.deferred) {
          comment.deferred.resolve();
        }
      }
    });
  }

  function subscribeToStatusLinks() {
    $(document).on('click', '.comment-actions .status', function() {
      var pullrequestId = $(this).data('pullrequestId');
      var id = $(this).data('id');
      var status = $(this).data('status');
      storage.sync(() => {
        var comments = storage.getComments(pullrequestId);
        comments[id] = status;
        storage.setComments(pullrequestId, comments).flush();
        Renderer.rerenderCommentActions(pullrequestId, id, status);
      });
    });
  }

  function subscribetoNavigationLinks() {
    $(document).on('click', '.comment-actions .next-comment', function() {
      var comment = $(this).parents('.comment')[0];
      var allComments = $('.comment');
      var index = allComments.index(comment) + 1;
      if (index >= allComments.length) {
        index = 0;
      }
      allComments[index].scrollIntoView({ behavior: "smooth", block: "center" });
    });
  }

  function subscribeToDomChanges() {
    var observer = new MutationObserver((mutations, observer) => {
      mutations.forEach((mutation) => {
        var addedNode = mutation.addedNodes[0];
        if (mutation.type == "childList" && mutation.target.tagName == "OL" && addedNode && addedNode.children[0] && addedNode.tagName == "LI" && addedNode.className == "comment") {
          Renderer.renderCommentActions(pullrequestId, $(addedNode).find('.iterable-item').data('commentId'), 'new');
        }
      });
    });
    observer.observe($('body')[0], {
      subtree: true,
      childList: true
    });
  }
})();
