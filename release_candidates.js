function icon(status) {
  if (status == 'new') {
    return 'far fa-star';
  } else if (status == 'candidate') {
    return 'fas fa-star-half-alt';
  } else {
    // released
    return 'fas fa-star';
  }
}

class ReleaseCandidates {
  constructor(storage) {
    this.storage = storage;
    window.storage = storage;
  }

  initIndexPage() {
    this.removeOldCandidates();
    var releaseCandidates = this.storage.getReleaseCandidates();
    $('table thead tr th:nth-child(3)').after('<th></th>');
    $('table tbody tr').each((_, row) => {
      var id = $(row).data('id');
      var releaseCandidate = releaseCandidates[id];
      var status;
      if (!releaseCandidate) {
        status = 'new';
      } else {
        status = releaseCandidate.status;
      }
      $(row).find('td:nth-child(3)').after(`<td data-id="${id}" data-status="${status}" class="relase-candidate-button"><i class="${icon(status)}"></i></td>`);
    });
  }

  subscribeToClick() {
    var _this = this;
    $(document).on('click', '.relase-candidate-button', function(e) {
      var status = $(this).data('status');
      var id = $(this).data('id');

      if (status == 'new') {
        _this.updateCandidateStatus(this, id, 'candidate');
      } else if (status == 'candidate') {
        _this.updateCandidateStatus(this, id, 'released');
      } else {
        // released
        _this.removeCandidateStatus(this, id);
      }
      storage.flush();
    });
  }

  updateCandidateStatus(el, id, newStatus) {
    this.updateDom(el, newStatus);
    this.storage.setReleaseCandidates(id, { updatedAt: new Date(), status: newStatus });
  }

  removeCandidateStatus(el, id) {
    this.updateDom(el, 'new');
    var releaseCandidates = this.storage.getReleaseCandidates();
    delete releaseCandidates[id];
    this.storage.setReleaseCandidates(null, releaseCandidates);
  }

  updateDom(el, newStatus) {
    $(el).data('status', newStatus);
    $(el).html(`<i class="${icon(newStatus)}"></i>`);
  }

  removeOldCandidates() {
    var millisecondsInDay = 86400000;
    var daysInMonth = 30;
    var releaseCandidates = this.storage.getReleaseCandidates();
    for(var candidateId in releaseCandidates) {
      if ((new Date() - new Date(releaseCandidates[candidateId].updatedAt)) / millisecondsInDay > daysInMonth) {
        delete releaseCandidates[candidateId];
      }
    }
    this.storage.setReleaseCandidates(null, releaseCandidates);
    this.storage.flush();
  }
}
