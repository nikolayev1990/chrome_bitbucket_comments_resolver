class CustomStorage {
  constructor() {
    this.cache = {
      comments: {},
      releaseCandidates: {}
    };
  }

  flush() {
    chrome.storage.sync.set(this.cache);
  }

  getComments(key) {
    return this._getValue('comments', key);
  }

  setComments(key, value) {
    return this._setValue('comments', key, value);
  }

  getReleaseCandidates(key) {
    return this._getValue('releaseCandidates', key);
  }

  setReleaseCandidates(key, value) {
    return this._setValue('releaseCandidates', key, value);
  }

  sync(callback) {
    chrome.storage.sync.get((object) => {
      $.extend(this.cache, object);
      callback();
    });
  }

  _getValue(objectName, key) {
    if (key) {
      return this._clone(this.cache[objectName][key]);
    } else {
      return this._clone(this.cache[objectName]);
    }
  }

  _setValue(objectName, key, value) {
    if (key) {
      this.cache[objectName][key] = this._clone(value);
    } else {
      this.cache[objectName] = this._clone(value);
    }
    return this;
  }

  _clone (value) {
    if (value) {
      return JSON.parse(JSON.stringify(value));
    } else {
      return value;
    }
  }
}
