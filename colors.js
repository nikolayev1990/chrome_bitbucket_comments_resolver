class Colors {
  static red(opacity = 1) {
    return `rgba(192, 57, 43, ${opacity})`;
  }
  static green(opacity = 1) {
    return `rgba(0, 135, 90, ${opacity})`;
  }
  static blue(opacity = 1) {
    return `rgba(155, 89, 182, ${opacity})`;
  }
}
