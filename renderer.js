class Renderer {
  static renderCommentActions(pullrequestId, id, status) {
    var actions;
    if (status == 'new') {
      actions = `<li class="custom-li"><a class="status seen" data-status="seen" data-pullrequest-id="${pullrequestId}" data-id="${id}" href="javascript:;" style="color: ${Colors.blue()};">Seen</a></li>
                 <li class="custom-li"><a class="status resolved" data-status="resolved" data-pullrequest-id="${pullrequestId}" data-id="${id}" href="javascript:;" style="color: ${Colors.green()};">Resolved</a></li>`
    } else if (status == 'seen') {
      actions = `<li class="custom-li"><a class="status new" data-status="new" data-pullrequest-id="${pullrequestId}" data-id="${id}" href="javascript:;" style="color: ${Colors.red()};">New</a></li>
                 <li class="custom-li"><a class="status resolved" data-status="resolved" data-pullrequest-id="${pullrequestId}" data-id="${id}" href="javascript:;" style="color: ${Colors.green()};">Resolved</a></li>`
    } else {
      actions = `<li class="custom-li"><a class="status new" data-status="new" data-pullrequest-id="${pullrequestId}" data-id="${id}" href="javascript:;" style="color: ${Colors.red()};">New</a></li>
                 <li class="custom-li"><a class="status seen" data-status="seen" data-pullrequest-id="${pullrequestId}" data-id="${id}" href="javascript:;" style="color: ${Colors.blue()};">Seen</a></li>`
    }
    actions = '<li class="custom-li"><a class="next-comment" href="javascript:;"">Next</a></li>' + actions;
    $(`#comment-${id}`).parent().removeClass('new seen resolved')
      .addClass(status).find('.comment-actions').first().prepend(actions);
  }

  static rerenderCommentActions(pullrequestId, id, status) {
    $(`#comment-${id} .comment-actions .custom-li`).remove();
    Renderer.renderCommentActions(pullrequestId, id, status);
  }

  static renderPullrequestCounters(pullrequestId, comments) {
    var newCommentsCount = Object.values(comments).filter((status) => status == 'new').length;
    var seenCommentsCount = Object.values(comments).filter((status) => status == 'seen').length;
    var resolvedCommentsCount = Object.values(comments).filter((status) => status == 'resolved').length;
    var counters = [];
    if (newCommentsCount > 0) counters.push(`<span style="color: ${Colors.red()}">${newCommentsCount}</span>`);
    if (seenCommentsCount > 0) counters.push(`<span style="color: ${Colors.blue()}">${seenCommentsCount}</span>`);
    if (resolvedCommentsCount > 0) counters.push(`<span style="color: ${Colors.green()}">${resolvedCommentsCount}</span>`);
    var html = `<div>${counters.join('/')}</div>`;
    $(`.pull-request-row-${pullrequestId} td:nth-child(2)>div>span>span:last-child`).html(html);
  }
}