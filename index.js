(function() {
  var indexUrl;
  var storage = new CustomStorage();
  var releaseCandidates = new ReleaseCandidates(storage);
  releaseCandidates.subscribeToClick();

  $(function() {
    match = window.location.pathname.match(/^\/?(?<account>[^\/]+)\/(?<repo>[^\/]+)\/pull-requests\/?$/);
    if (match == null) {
      return;
    }

    indexUrl = `https://bitbucket.org/!api/2.0/repositories/${match.groups.account}/${match.groups.repo}/pullrequests?pagelen=50&q=state="OPEN"`;
    storage.sync(() => {
      subscribeToDomChanges();
    });
  });

  function getPullrequests(url) {
    $.ajax({
      method: "GET",
      url: url,
      success: function(data) {
        if (data.next) {
          getPullrequests(data.next);
        }

        removeClosedPullrequestsFromStorage(data.values);

        data.values.forEach((pullrequest) => {
          if (pullrequest.comment_count > 0) {
            commentsUrl = URI(pullrequest.links.comments.href).addSearch('pagelen', 100);
            getComments(pullrequest, commentsUrl);
          }
        });
      }
    });
  }

  function getComments(pullrequest, url) {
    $.ajax({
      method: "GET",
      url: url,
      success: function(data) {
        if (data.next) {
          getComments(pullrequest, URI(data.next).addSearch('pagelen', 100));
        }
        var statuses = storage.getComments(pullrequest.id);
        if (statuses == null) {
          statuses = {};
        }
        data.values.forEach((comment) => {
          if (statuses[comment.id] == null) statuses[comment.id] = 'new';
          if (comment.deleted) delete statuses[comment.id];
        });
        storage.setComments(pullrequest.id, statuses);
        if (!data.next) {
          storage.flush();
          Renderer.renderPullrequestCounters(pullrequest.id, statuses);
        }
      }
    });
  }

  function removeClosedPullrequestsFromStorage(values) {
    var pullrequests = storage.getComments();
    for (var pullrequestId in pullrequests) {
      var found = values.find(function(pullrequest) {
        return pullrequestId == pullrequest.id;
      });
      if (!found) {
        delete pullrequests[pullrequestId];
      }
    }
    storage.setComments(null, pullrequests).flush();
  }

  function subscribeToDomChanges() {
    var observer = new MutationObserver((mutations, observer) => {
      mutations.forEach((mutation) => {
        if (mutation.type == "childList" && mutation.target.tagName == "DIV" && mutation.addedNodes[0] && mutation.addedNodes[0].children[0] && mutation.addedNodes[0].children[0].tagName == "TABLE") {
          setTableRowIds(mutation.target);
          getPullrequests(indexUrl);
          releaseCandidates.initIndexPage();
        }
      });
    });
    observer.observe($('body')[0], {
      subtree: true,
      childList: true
    });
  }

  function setTableRowIds(container) {
    $(container).find('tbody tr').each((_, row) => {
      var id = row.innerHTML.match(/\- #(?<id>\w+),/).groups.id;
      $(row).addClass(`pull-request-row-${id}`).data('id', id);
    });
  }
})();